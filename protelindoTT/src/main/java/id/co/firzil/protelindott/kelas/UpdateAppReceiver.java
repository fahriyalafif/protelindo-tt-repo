package id.co.firzil.protelindott.kelas;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.firebase.iid.FirebaseInstanceId;

public class UpdateAppReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(final Context c, Intent in) {
		if(in.getAction().equalsIgnoreCase(Intent.ACTION_MY_PACKAGE_REPLACED)){
			String regid = FirebaseInstanceId.getInstance().getToken();
			if(regid == null) regid = "";
			System.out.println("update app token: " + regid);

			new FcmPreference(c).setIsRegisteredInServer(false);
			new Me(c).setGcmRegid(regid);

			try{
				AsyncProcess ap = new AsyncProcess(c, "");
				ap.showProgressDialog(false);
				ap.setOnAksesData(new AsyncProcess.OnAksesData() {
					@Override
					public void onDoInBackground() {
						new UpdateRegid(c).updateSync();
					}

					@Override
					public void onPostExecute() {

					}

					@Override
					public void onPreExecute() {

					}
				});
				ap.jalankan();
			}
			catch (Exception e){
				e.printStackTrace();
				new UpdateRegid(c).updateSync();
			}
		}
	}

}
