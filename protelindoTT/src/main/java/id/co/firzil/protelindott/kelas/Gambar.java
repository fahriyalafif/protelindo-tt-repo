package id.co.firzil.protelindott.kelas;

public class Gambar {
	private boolean edited = false;
	private String path, idserver = "", caption="";
	private int idlokal = 0, order = 0, used_flag = 0;
	private Object tag;	
	
	public static final int NO_UPDATE=0, IMAGE_UPDATE=1, NOT_IMAGE_UPDATE=2;
	
	public Gambar(){}
	public Gambar(String tag){this.tag = tag;}
		
	public void setPath(String path){
		this.path = path;
	}
	
	public void setIdServer(String idserver){
		this.idserver = idserver;
	}
	
	public void setIdLokal(int idlokal){
		this.idlokal = idlokal;
	}
	
	public void setImageEdited(boolean edited){
		this.edited = edited;
	}
	
	public Gambar setTag(Object tag){
		this.tag = tag;
		return this;
	}
	
	public String getPath(){
		return path;
	}
	
	public String getIdServer(){
		return idserver;
	}
	
	public int getIdLokal(){
		return idlokal;
	}
	
	public boolean isImageEdited(){
		return edited;
	}
	
	public boolean isFromLokal(){
		return idlokal > 0;
	}
	
	public Object getTag(){
		return tag;
	}

	public void setOrder(int order){
		this.order = order;
	}

	public void setUsedFlag(int used_flag){
		this.used_flag = used_flag;
	}

	public int getOrder(){
		return order;
	}

	public int getUsedFlag(){
		return used_flag;
	}

	public void setCaption(String caption){
		this.caption = caption;
	}

	public String getCaption(){
		return caption;
	}

}
