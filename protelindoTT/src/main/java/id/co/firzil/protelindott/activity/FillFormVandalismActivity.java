package id.co.firzil.protelindott.activity;

import org.json.JSONArray;
import org.json.JSONObject;

import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.kelas.AddDetailTenantLayout;
import id.co.firzil.protelindott.kelas.AksesData;
import id.co.firzil.protelindott.kelas.AsyncProcess;
import id.co.firzil.protelindott.kelas.Constants;
import id.co.firzil.protelindott.kelas.DbHandler;
import id.co.firzil.protelindott.kelas.Delivery;
import id.co.firzil.protelindott.kelas.Flag;
import id.co.firzil.protelindott.kelas.TTApproval;
import id.co.firzil.protelindott.kelas.URL;
import id.co.firzil.protelindott.kelas.Utils;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.LinearLayout.LayoutParams;

@SuppressLint("InflateParams")
public class FillFormVandalismActivity extends BaseFragmentActivity{
	private TableLayout layout_tenant, layout_damaged, layout_record;
	private View add_damage, add_record, add_edit_pict, save;
	private LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
	private String idtt, old_improve="", old_security="", old_site="";
	private EditText remark, tenant, asset_owner, improvement, security, site_surrounding, ttdate, vandalism_report;
	private RadioGroup group_lost;
	private AddDetailTenantLayout a = new AddDetailTenantLayout();
	
	protected void onCreate(Bundle b){
		super.onCreate(b);
		
		setJudul("TT Vandalism");
		
		View m = inf.inflate(R.layout.fill_form_vandalism, null);
		lp.setMargins(0, 0, 0, Utils.dpToPixel(c, 10));
		
		layout_tenant = (TableLayout) m.findViewById(R.id.detail_tenant);
		layout_damaged = (TableLayout) m.findViewById(R.id.layout_damaged);
		layout_record = (TableLayout) m.findViewById(R.id.layout_track_record);
		remark = (EditText) m.findViewById(R.id.remark);
		asset_owner = (EditText) m.findViewById(R.id.asset_owner);
		improvement = (EditText) m.findViewById(R.id.improvement);
		security = (EditText) m.findViewById(R.id.security);
		site_surrounding = (EditText) m.findViewById(R.id.site_surrounding);
		ttdate = (EditText) m.findViewById(R.id.tt_date);
		vandalism_report = (EditText) m.findViewById(R.id.vandalism_report);
		group_lost = (RadioGroup) m.findViewById(R.id.group_lost);
		add_damage = m.findViewById(R.id.add_damage);
		add_record = m.findViewById(R.id.add_track_record);
		tenant = (EditText) m.findViewById(R.id.tenant);
		save = m.findViewById(R.id.save);
		add_edit_pict = m.findViewById(R.id.add_edit_pict);
		
		add_edit_pict.setOnClickListener(this);
		add_damage.setOnClickListener(this);
		add_record.setOnClickListener(this);
		save.setOnClickListener(this);
		
		
		data_tt = getIntent().getStringExtra(Constants.DATA_TT);
		try{
			JSONObject j = new JSONObject(data_tt);
			idtt = j.getString("idtt");
			
			String data_tt = getIntent().getStringExtra(Constants.DATA_TT);
			JSONObject o = new JSONObject(data_tt);
			add("Site ID", o.getString("protelindo_site_id"));
			add("Site Name", o.getString("protelindo_site_name"));
			add("TT Date", o.getString("trouble_ticket_date"));
			add("Resolution Target", o.optString("resolution_target").replace("null", ""));
			add("Tenant", o.getString("tenant"));
			add("TT Number", o.getString("tt_no"));
			
			JSONObject data_pic = o.getJSONObject("data_pic");
			add("Vendor Name", o.getString("OMSubcont").equalsIgnoreCase("null") ? "" : o.getString("OMSubcont"));
			add("PIC Name", data_pic.getString("fullname"));
			add("PIC Phone", data_pic.getString("phone"));
		
		}
		catch(Exception e){
			e.printStackTrace();
			notif_error_parsing_data_tt();
		}
		
		setMainLayout(m);
		
		runAsyncProcessBackground("Getting data..", new AsyncProcess.OnAksesData() {
			private JSONObject json_meta, json_damage/*, json_histori*/;
			private boolean damage_server = false, meta_server = false;
			
			@Override
			public void onPreExecute() {}
			
			@Override
			public void onPostExecute() {
				try{
					if(json_meta != null && json_meta.optInt("flag") == 1){
						JSONArray ar = json_meta.optJSONArray("data_meta");
						if(ar != null && ar.length() > 0){
							for(int i=0; i<ar.length(); i++){
								JSONObject o = ar.getJSONObject(i);
								String n = o.getString("meta_name");
								String v = o.getString("meta_value");
								String t = o.getString("meta_input_type");

								Log.d("META VANDALISM", "META VANDALISM "+n+" : "+v);
								
								if(n.equalsIgnoreCase("improvement")) {
									improvement.setText(v);
									old_improve = v;
								}
								else if(n.equalsIgnoreCase("security_system")) {
									security.setText(v);
									old_security = v;
								}
								else if(n.equalsIgnoreCase("site_and_surrounding")) {
									site_surrounding.setText(v);
									old_site = v;
								}
								
								if(meta_server){
									DbHandler.get(c).insertAndUpdateMeta(idtt, n, v, t, "", Delivery.SUCCEED, Flag.VANDALISM, getLat(), getLon());
								}
							}
						}
					}
				}
				catch(Exception e){
					e.printStackTrace();
					if(online()) notif("Error getting meta data");
				}
				
				try{
					if(json_damage != null && json_damage.optInt("flag") == 1){
						JSONArray ar = json_damage.optJSONArray("data");
						if(ar != null && ar.length() > 0){
							for(int i=0; i<ar.length(); i++){
								JSONObject ob = ar.getJSONObject(i);
								String owner = ob.getString("asset_owner"), tenant = ob.getString("tenant"), remark = ob.getString("remark");
								addDamaged(owner, tenant, remark);
								
								if(damage_server){
									DbHandler.get(c).insertDamaged(idtt, owner, remark, tenant, "", Delivery.SUCCEED);
								}
							}
						}
					}
				}
				catch(Exception e){
					e.printStackTrace();
					if(online()) notif("Error getting damage data");
				}
				
				/*try{
					JSONArray ar = json_histori.optJSONArray("data");
					if(json_damage.optInt("flag") == 1 && ar != null && ar.length() > 0){
						for(int i=0; i<ar.length(); i++){
							JSONObject ob = ar.getJSONObject(i);
							addTrackRecord(ob.getString("record_date"), ob.getInt("bst_asset") == 1 ? "Yes" : "No", ob.getString("record_description"));
						}
					}
				}
				catch(Exception e){
					e.printStackTrace();
					notif("Error getting history record");
				}*/
			}
			
			@Override
			public void onDoInBackground() {
				DbHandler db = DbHandler.get(c);
				
				Cursor cc = db.getLastDamaged(idtt);
				if(cc.getCount() > 0){
					try{
						json_damage = new JSONObject();
						json_damage.put("flag", 1);
						JSONArray arr = new JSONArray();
						while(cc.moveToNext()){
							JSONObject o = new JSONObject();
							o.put("asset_owner", cc.getString(0));
							o.put("tenant", cc.getString(1));
							o.put("remark", cc.getString(2));
							arr.put(o);
						}
						json_damage.put("data", arr);
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
				else if(online() && TTApproval.isHadBeenRejected(data_tt)){
					AksesData ak = new AksesData();
					ak.addParam("idtt", idtt);
					json_damage = ak.getJSONObjectRespon(URL.ALL_DAMAGED_ITEM, "GET");
					damage_server = true;
				}
				cc.close();
				
				cc = db.getAllMeta(idtt, Flag.VANDALISM);
				if(cc.getCount() > 0){
					try{
						json_meta = new JSONObject();
						json_meta.put("flag", 1);
						JSONArray arr = new JSONArray();
						while(cc.moveToNext()){
							JSONObject o = new JSONObject();
							o.put("meta_name", cc.getString(2));
							o.put("meta_value", cc.getString(3));
							o.put("meta_input_type", cc.getString(4));
							arr.put(o);
						}
						json_meta.put("data_meta", arr);
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
				else if(online() && TTApproval.isHadBeenRejected(data_tt)){
					AksesData ak = new AksesData();
					ak.addParam("type", "tt");
					ak.addParam("id", idtt);
					json_meta = ak.getJSONObjectRespon(URL.ALL_META, "GET");
					meta_server = true;
				}
				cc.close();
				
				/*ak = new AksesData();
				ak.addParam("idtt", idtt);
				json_histori = ak.getJSONObjectRespon(URL.ALL_VANDALISM_RECORD, "GET");*/
			}
		});
	}
	
	private void add(String name, String value){
		a.add(c, layout_tenant, name, value);
	}
	
	private void addDamaged(String owner, String tenant, String remark){
		LinearLayout ll = new LinearLayout(c);
		ll.setLayoutParams(lp);
		ll.setOrientation(LinearLayout.VERTICAL);
		ll.setBackgroundResource(R.drawable.shape_putih_abu);
		layout_damaged.addView(ll);
		
		addDetailDamaged(ll, "Owner", owner);
		addDetailDamaged(ll, "Tenant", tenant);
		addDetailDamaged(ll, "Remark", remark);
	}
	
	private void addDetailDamaged(LinearLayout ll, String name, String value){
		a.add(c, ll, name, value);
	}
	
	private void addTrackRecord(String ttdate, String lost, String report){
		LinearLayout ll = new LinearLayout(c);
		ll.setLayoutParams(lp);
		ll.setOrientation(LinearLayout.VERTICAL);
		ll.setBackgroundResource(R.drawable.shape_putih_abu);
		layout_record.addView(ll);
		
		addDetailDamaged(ll, "TT Date", ttdate);
		addDetailDamaged(ll, "Any Lost", lost);
		addDetailDamaged(ll, "Report", report);
	}
	
	public void onClick(View v){
		if(v == add_edit_pict){
			startActivity(new Intent(c, AddEditPictVandalismActivity.class).putExtra(Constants.DATA_TT, data_tt));
		}
		else if(v == add_damage){
			String sowner = asset_owner.getText().toString(), 
					stenant = tenant.getText().toString(), 
					sremark = remark.getText().toString(),
					datetime = time;
			DbHandler.get(c).insertDamaged(idtt, sowner, sremark, stenant, datetime, Delivery.PENDING);
			DbHandler.get(c).insertAndUpdateMeta(idtt, Flag.VANDALISM_DAMAGE, "1", "text", datetime, Delivery.PENDING, Flag.VANDALISM_DAMAGE, getLat(), getLon());
			DbHandler.get(c).deleteMetaByIdtt(idtt, Flag.AC_);

			asset_owner.setText("");
			tenant.setText("");
			remark.setText("");
			
			addDamaged(sowner, stenant, sremark);
		}
		else if(v == add_record){
			runAsyncProcess("Adding history record..", new AsyncProcess.OnAksesData() {
				String sttdate = ttdate.getText().toString(), 
						sreport = vandalism_report.getText().toString(), 
						sanylost = ((RadioButton) group_lost.findViewById(group_lost.getCheckedRadioButtonId())).getText().toString().equalsIgnoreCase("yes") ? "1" : "0",
						datetime = time;
				JSONObject j;
				
				@Override
				public void onPreExecute() {}
				
				@Override
				public void onPostExecute() {
					try{
						if(j.optInt("flag") == 1){
							notif("history record added");
							addTrackRecord(sttdate, ((RadioButton) group_lost.findViewById(group_lost.getCheckedRadioButtonId())).getText().toString(), sreport);
						}
						else notif("failed add history record");
					}
					catch(Exception e){
						e.printStackTrace();
						notif_server_error();
					}
				}
				
				@Override
				public void onDoInBackground() {
					AksesData ak = new AksesData();
					ak.addParam("idtt", idtt);
					ak.addParam("record_date", sttdate);
					ak.addParam("record_description", sreport);
					ak.addParam("bst_asset", sanylost);
					ak.addParam("datetime", datetime);
					j = ak.getJSONObjectRespon(URL.SAVE_HISTORI, "POST");
				}
			});
		}
		else if(v == save){
			String time = this.time;
			DbHandler db = DbHandler.get(c);
			
			if(! Utils.sama(old_improve, improvement.getText().toString()))
				db.insertAndUpdateMeta(idtt, "improvement", improvement.getText().toString(), "text", time, Delivery.PENDING, Flag.VANDALISM, getLat(), getLon());
			
			if(! Utils.sama(old_security, security.getText().toString()))
				db.insertAndUpdateMeta(idtt, "security_system", security.getText().toString(), "text", time, Delivery.PENDING, Flag.VANDALISM, getLat(), getLon());
			
			if(! Utils.sama(old_site, site_surrounding.getText().toString()))
				db.insertAndUpdateMeta(idtt, "site_and_surrounding", site_surrounding.getText().toString(), "text", time, Delivery.PENDING, Flag.VANDALISM, getLat(), getLon());
			
			db.insertAndUpdateMeta(idtt, Flag.VANDALISM, "1", "text", time, Delivery.PENDING, Flag.VANDALISM, getLat(), getLon());
			db.deleteMetaByIdtt(idtt, Flag.AC_);
			
			notif("Data saved");
			
			startActivityAndFinish(new Intent(c, OpenFormActivity.class).putExtra(Constants.DATA_TT, data_tt));
		}
		else super.onClick(v);
	}
	
	public void onBackPressed(){
		startActivityAndFinish(new Intent(c, OpenFormActivity.class).putExtra(Constants.DATA_TT, data_tt));
	}
	
}
