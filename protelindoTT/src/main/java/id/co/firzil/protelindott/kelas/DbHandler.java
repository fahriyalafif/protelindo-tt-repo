package id.co.firzil.protelindott.kelas;

import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

public class DbHandler extends SQLiteOpenHelper {

	private static DbHandler main;
	private static SQLiteDatabase db;
	private static final int DB_VERSION = 1;
	private static final String DB_NAME = "protelindott.db", ID_LOKAL="id_ne", LAT="lat", LON="lon", TT_ID="idtt",
			TT_JSON="tt_json", TT_TYPE="tt_type", TT_CASE="tt_case", TT_KRONOLOGI="tt_kronologi", TT_DESC="tt_description", 
			TT_STATUS="sla_event_status", TT_SUBMISSION_TIME="submission_time", DELIVERY="delivery", SUBMIT_STATUS="submit_status", TT_MSG="message";
	
	public static final String TABEL_OV="tt_overview", TABEL_TT="tt", TABEL_GENERAL="tt_general", TABEL_DAMAGED="damaged", 
			TABEL_META="meta", TABEL_VANDALISM_PICTURE="vandalism_picture", TABEL_LOG_LATLON="log_lat_lon", TABEL_NOTIF = "notif";
	
	private static final String OV_COUNT="count", OV_STATUS="sla_event_status";
	private static final String GN_TT="idtt", GN_TT_GENERAL="idttgeneral", GN_CAPTION="caption", GN_DESC="desc",
			GN_PICTURE="picture", GN_DATETIME="datetime", GN_PICT_SERVER="pict_server", GN_UPDATE_STATUS = "update_status",
			ORDER="urutan", USED_FLAG="used_flag";  //0 = tidak update, 1=full update, 2=caption n desc update
	private static final String VD_TT="idtt", VD_PICTURE_ID="idvandalism_picture", VD_CAPTION="caption", VD_DESC="desc", 
			VD_PICTURE="picture", VD_DATETIME="datetime", VD_UPDATE_STATUS="update_status";
	private static final String DM_TT="idtt", DM_OWNER="asset_owner", DM_TENANT="tenant", DM_REMARK="remark", DM_DATETIME="datetime";
	private static final String MT_TT="idtt", MT_NAME="name", MT_VALUE="value", MT_TYPE="type", MT_DATETIME="datetime", MT_FLAG="flag";
	private static final String LOG_SITE_ID="siteid", LOG_REF_NO="ref_no", LOG_LAT_ORI="lat_ori", LOG_LON_ORI="lon_ori", LOG_LAT_POSISI="lat_pos",
			LOG_LON_POSISI="lon_pos", LOG_JARAK="jarak", LOG_DATETIME="datetime", LOG_APK_VERSION="apk_version", LOG_ANDROID_VERSION="android_version",
			LOG_MODEL_NUMBER="model_number", LOG_KERNEL_NUMBER="kernel_number", LOG_BUILD_NUMBER="build_number", LOG_IMEI="imei", LOG_CREATED_BY="created_by";
	private static final String ID_NOTIF = "id_notif";

	public synchronized long insertLog(String siteid, String refno, String latori, String lonori, String latpos, String lonpos, String jarak,
									   String datetime, String apkversion, String androidversion, String modelnumber, String kernelnumber,
									   String buildnumber, String imei, String createdby){
		ContentValues cv = new ContentValues();
		cv.put(LOG_SITE_ID, siteid);
		cv.put(LOG_REF_NO, refno);
		cv.put(LOG_LAT_ORI, latori);
		cv.put(LOG_LON_ORI, lonori);
		cv.put(LOG_LAT_POSISI, latpos);
		cv.put(LOG_LON_POSISI, lonpos);
		cv.put(LOG_JARAK, jarak);
		cv.put(LOG_DATETIME, datetime);
		cv.put(LOG_APK_VERSION, apkversion);
		cv.put(LOG_ANDROID_VERSION, androidversion);
		cv.put(LOG_MODEL_NUMBER, modelnumber);
		cv.put(LOG_KERNEL_NUMBER, kernelnumber);
		cv.put(LOG_BUILD_NUMBER, buildnumber);
		cv.put(LOG_IMEI, imei);
		cv.put(LOG_CREATED_BY, createdby);

		return db.insert(TABEL_LOG_LATLON, null, cv);
	}

	public synchronized int updateLogDelivery(int idlokal, int delivery){
		ContentValues cv = new ContentValues();
		cv.put(DELIVERY, delivery);
		return db.update(TABEL_LOG_LATLON, cv, ID_LOKAL + "=?", new String[]{idlokal + ""});
	}

	public synchronized int deleteLog(int idlokal){
		return db.delete(TABEL_LOG_LATLON, ID_LOKAL + "=?", new String[]{idlokal + ""});
	}

	public synchronized Cursor getLog(int delivery){
		return db.query(TABEL_LOG_LATLON, null, DELIVERY + "=?", new String[]{delivery + ""}, null, null, ID_LOKAL + " ASC");
	}

	private DbHandler(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	public static DbHandler get(Context c){
		if(main == null) {
			main = new DbHandler(c);
			main.open();
		}
		return main;
	}

	private void open(){
		db = getWritableDatabase();
	}
	
	@Override
	public synchronized void onCreate(SQLiteDatabase db) {
		String notif = "CREATE TABLE "+TABEL_NOTIF+" (" +
				ID_LOKAL + " INTEGER PRIMARY KEY, " +
				ID_NOTIF + " INTEGER )";

		db.execSQL(notif);

		String log = "CREATE TABLE "+TABEL_LOG_LATLON+" (" +
				ID_LOKAL + " INTEGER PRIMARY KEY, " +
				LOG_SITE_ID + " TEXT, " +
				LOG_REF_NO + " TEXT, " +
				LOG_LAT_ORI + " TEXT, " +
				LOG_LON_ORI + " TEXT, " +
				LOG_LAT_POSISI + " TEXT, " +
				LOG_LON_POSISI + " TEXT, " +
				LOG_JARAK + " TEXT, " +
				LOG_DATETIME + " TEXT, " +
				LOG_APK_VERSION + " TEXT, " +
				LOG_ANDROID_VERSION + " TEXT, " +
				LOG_MODEL_NUMBER + " TEXT, " +
				LOG_KERNEL_NUMBER + " TEXT, " +
				LOG_BUILD_NUMBER + " TEXT, " +
				LOG_IMEI + " TEXT, " +
				LOG_CREATED_BY + " TEXT, " +
				DELIVERY + " INTEGER DEFAULT "+Delivery.PENDING+" )";

		db.execSQL(log);

		String tt = "CREATE TABLE "+TABEL_TT+" (" + 
				ID_LOKAL + " INTEGER PRIMARY KEY, " + 
				TT_ID + " TEXT, " + 
				TT_JSON + " TEXT, "	+
				TT_TYPE + " TEXT, " + 
				TT_CASE + " TEXT, " +
				TT_KRONOLOGI + " INTEGER DEFAULT 0, " +
				TT_DESC + " TEXT, "+
				TT_STATUS + " TEXT, "+
				TT_SUBMISSION_TIME + " TEXT, "+
				DELIVERY + " INTEGER DEFAULT "+Delivery.SUCCEED+", " + 
				TT_MSG + " TEXT )";
		
		db.execSQL(tt);
		
		String ov = "CREATE TABLE "+TABEL_OV+" (" + 
				ID_LOKAL + " INTEGER PRIMARY KEY, " + 
				OV_STATUS + " TEXT, " + 
				OV_COUNT + " INTEGER )";
		
		db.execSQL(ov);
		
		String gn = "CREATE TABLE "+TABEL_GENERAL+" (" + 
				ID_LOKAL + " INTEGER PRIMARY KEY, " + 
				GN_TT + " TEXT, " +
				GN_CAPTION + " TEXT, " +
				GN_DESC + " TEXT, " + 
				GN_PICTURE + " TEXT, " + 
				GN_DATETIME + " TEXT, " +
				GN_PICT_SERVER + " INTEGER DEFAULT 0, " +
				DELIVERY + " INTEGER DEFAULT "+Delivery.SUCCEED+", "+ 
				GN_TT_GENERAL + " TEXT, " + 
				GN_UPDATE_STATUS + " INTEGER DEFAULT "+Gambar.NO_UPDATE+", "+
				SUBMIT_STATUS + " TEXT, " +
				LAT + " TEXT, "+
				LON + " TEXT, "+
				ORDER + " INTEGER DEFAULT 0, "+
				USED_FLAG + " INTEGER DEFAULT 0)";
				
		db.execSQL(gn);
		
		String dm = "CREATE TABLE "+TABEL_DAMAGED+" (" + 
				ID_LOKAL + " INTEGER PRIMARY KEY, " + 
				DM_TT + " TEXT, " +
				DM_OWNER + " TEXT, " +
				DM_TENANT + " TEXT, " + 
				DM_REMARK + " TEXT, " + 
				DM_DATETIME + " TEXT, " + 
				DELIVERY + " INTEGER DEFAULT "+Delivery.SUCCEED+" )";
		
		db.execSQL(dm);
		
		String mt = "CREATE TABLE "+TABEL_META+" (" + 
				ID_LOKAL + " INTEGER PRIMARY KEY, " + 
				MT_TT + " TEXT, " +
				MT_NAME + " TEXT, " +
				MT_VALUE + " TEXT, " + 
				MT_TYPE + " TEXT, " + 
				MT_DATETIME + " TEXT, " + 
				DELIVERY + " INTEGER DEFAULT "+Delivery.SUCCEED+", "+ 
				MT_FLAG + " TEXT, " +
				LAT + " TEXT, "+
				LON + " TEXT )";
		
		db.execSQL(mt);
		
		String vd = "CREATE TABLE "+TABEL_VANDALISM_PICTURE+" (" + 
				ID_LOKAL + " INTEGER PRIMARY KEY, " + 
				VD_TT + " TEXT, " +
				VD_CAPTION + " TEXT, " +
				VD_DESC + " TEXT, " + 
				VD_PICTURE + " TEXT, " + 
				VD_DATETIME + " TEXT, " +
				VD_PICTURE_ID + " TEXT, " +
				DELIVERY + " INTEGER DEFAULT "+Delivery.SUCCEED+", " +
				VD_UPDATE_STATUS + " INTEGER DEFAULT "+Gambar.NO_UPDATE+", "+
				SUBMIT_STATUS + " TEXT, " +
				LAT + " TEXT, "+
				LON + " TEXT, " +
				ORDER + " INTEGER DEFAULT 0, "+
				USED_FLAG + " INTEGER DEFAULT 0)";

		db.execSQL(vd);

	}

	@Override
	public synchronized void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public synchronized void insertNotif(int id_notif){
        if(db.query(TABEL_NOTIF, new String[]{ID_NOTIF}, ID_NOTIF + "=?", new String[]{id_notif + ""}, null, null, null).getCount() == 0){
            ContentValues cv = new ContentValues();
            cv.put(ID_NOTIF, id_notif);
            db.insert(TABEL_NOTIF, "", cv);
        }
    }

    public synchronized Cursor getAllNotif(){
        return db.query(TABEL_NOTIF, new String[]{ID_NOTIF}, null, null, null, null, null);
    }

	public synchronized void insertAndUpdateDataTT(String idtt, String json){
		ContentValues cv = new ContentValues();
		cv.put(TT_JSON, json);

		Cursor cc = db.query(TABEL_TT, new String[]{TT_JSON}, TT_ID+"=?", new String[]{idtt}, null, null, null);

		if(cc.moveToNext()){  //jika sudah ada
			try{
				String json_lokal = cc.getString(0);
				String status_lokal = new JSONObject(json_lokal).getString("tt_current_status");
				String status_server = new JSONObject(json).getString("tt_current_status");

				if(! status_lokal.equalsIgnoreCase(status_server)){   //jika status di lokal != di server maka di update
					cv.put(TT_CASE, "");
					cv.put(TT_TYPE, "");
					cv.put(TT_KRONOLOGI, 0);
					cv.put(TT_DESC, "");
				}
				db.update(TABEL_TT, cv, TT_ID+"=?", new String[]{idtt});

			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		else{  //jika belum ada di insert
			try{
				cv.put(TT_STATUS, new JSONObject(json).getString("sla_event_status").toLowerCase());
			}
			catch(Exception e){
				e.printStackTrace();
			}

			cv.put(TT_ID, idtt);

			db.insert(TABEL_TT, "", cv);
		}
		cc.close();
	}
	
	public synchronized void updateTTMsg(String idtt, String error_msg){
		ContentValues cv = new ContentValues();
		cv.put(TT_MSG, error_msg);
		db.update(TABEL_TT, cv, TT_ID + "=?", new String[]{idtt});
	}
	
	public synchronized void emptyTable(String table){
		db.delete(table, null, null);
	}
	
	public synchronized long insertDataGeneral(String idtt, String caption, String desc, String picture, String datetime, int delivery, String idttgeneral, int status, String submit_status,
								  double lat, double lon, int order, int used_flag){
		
		ContentValues cv = new ContentValues();
		cv.put(GN_TT, idtt);
		cv.put(GN_CAPTION, caption);
		cv.put(GN_DESC, desc);
		cv.put(GN_PICTURE, picture);
		cv.put(GN_DATETIME, datetime);
		cv.put(DELIVERY, delivery);
		cv.put(GN_TT_GENERAL, idttgeneral);
		cv.put(GN_UPDATE_STATUS, status);
		cv.put(SUBMIT_STATUS, submit_status);
		cv.put(LAT, lat);
		cv.put(LON, lon);
		cv.put(ORDER, order);
		cv.put(USED_FLAG, used_flag);
		return db.insert(TABEL_GENERAL, "", cv);
	}
	
	public synchronized void updateDataGeneral(int id_lokal, String caption, String picture,
								  int delivery, int order, int used_flag){
		
		ContentValues cv = new ContentValues();
		cv.put(GN_CAPTION, caption);
		cv.put(GN_PICTURE, picture);
		cv.put(DELIVERY, delivery);
		cv.put(ORDER, order);
		cv.put(USED_FLAG, used_flag);
		db.update(TABEL_GENERAL, cv, ID_LOKAL + "=?", new String[]{id_lokal + ""});
	}

	public synchronized void updateImageGeneral(int id_lokal, String picture, String datetime, double lat, double lon){
		
		ContentValues cv = new ContentValues();
		cv.put(GN_PICTURE, picture);
		cv.put(GN_DATETIME, datetime);
		cv.put(GN_UPDATE_STATUS, Gambar.IMAGE_UPDATE);
		cv.put(DELIVERY, Delivery.PENDING);
		cv.put(LAT, lat);
		cv.put(LON, lon);
		db.update(TABEL_GENERAL, cv, ID_LOKAL+"=?", new String[]{id_lokal+""});

	}

	public synchronized void updateVandalismPictOrder(int id_lokal, int order){

		ContentValues cv = new ContentValues();
		cv.put(DELIVERY, Delivery.PENDING);
		cv.put(ORDER, order);
		db.update(TABEL_VANDALISM_PICTURE, cv, ID_LOKAL + "=?", new String[]{id_lokal + ""});

		cv = new ContentValues();
		cv.put(VD_UPDATE_STATUS, Gambar.NOT_IMAGE_UPDATE);
		db.update(TABEL_VANDALISM_PICTURE, cv, ID_LOKAL + "=? AND " + VD_UPDATE_STATUS + "=?", new String[]{id_lokal + "", Gambar.NO_UPDATE + ""});

	}

	public synchronized void updateImageGeneralOrder(int id_lokal, int order){
		
		ContentValues cv = new ContentValues();
		cv.put(DELIVERY, Delivery.PENDING);
		cv.put(ORDER, order);
		db.update(TABEL_GENERAL, cv, ID_LOKAL + "=?", new String[]{id_lokal + ""});

		cv = new ContentValues();
		cv.put(GN_UPDATE_STATUS, Gambar.NOT_IMAGE_UPDATE);
		db.update(TABEL_GENERAL, cv, ID_LOKAL + "=? AND " + GN_UPDATE_STATUS + "=?", new String[]{id_lokal + "", Gambar.NO_UPDATE + ""});

	}
	
	public synchronized void updateStatusUpdateGeneral(int id_lokal, int update){
		
		ContentValues cv = new ContentValues();
		cv.put(GN_UPDATE_STATUS, update);
		db.update(TABEL_GENERAL, cv, ID_LOKAL + "=?", new String[]{id_lokal + ""});

	}
	
	public synchronized void deleteDataGeneralByIdLokal(int idlokal) {
		db.delete(TABEL_GENERAL, ID_LOKAL + "=?", new String[]{idlokal + ""});
	}

	public synchronized void deleteDataGeneralByIdtt(String idtt) {
		db.delete(TABEL_GENERAL, GN_TT+"=?", new String[]{idtt});
	}
	
	public synchronized void deleteVandalismPictureByIdLokal(int idlokal) {
		db.delete(TABEL_VANDALISM_PICTURE, ID_LOKAL+"=?", new String[]{idlokal+""});
	}
	
	public synchronized void deleteVandalismPictureByIdtt(String idtt) {
		db.delete(TABEL_VANDALISM_PICTURE, VD_TT + "=? AND ( " + DELIVERY + "=? OR " + DELIVERY + "=? )", new String[]{idtt, Delivery.SUCCEED+"", Delivery.FAILED+""});
	}
	
	public synchronized void deleteVandalismDamageByIdtt(String idtt){
		db.delete(TABEL_DAMAGED, DM_TT+"=? AND ( "+DELIVERY+"=? OR "+DELIVERY+"=? )", new String[]{idtt, Delivery.SUCCEED+"", Delivery.FAILED+""});
	}
	
	public synchronized Cursor getDataGeneral(String idtt, int delivery, String submit_status){
		return db.query(TABEL_GENERAL, null, GN_TT+"=? AND "+DELIVERY+"=? AND "+SUBMIT_STATUS+"=?", new String[]{idtt, delivery+"", submit_status}, null, null, ID_LOKAL+" ASC");
	}
	
	public synchronized Cursor getSingleDataGeneral(String idtt, int idlokal){
		return db.query(TABEL_GENERAL, null, GN_TT+"=? AND "+ID_LOKAL+"=?", new String[]{idtt, idlokal+""}, null, null, ID_LOKAL+" ASC");
	}
	
	public synchronized Cursor getDataGeneral(String idtt, String submit_status){
		return db.query(TABEL_GENERAL, null, GN_TT+"=? AND "+SUBMIT_STATUS+"=?", new String[]{idtt, submit_status}, null, null, ORDER+" ASC");
	}

	public synchronized Cursor getDataGeneralWithImage(String idtt, String submit_status){
		return db.query(TABEL_GENERAL, null, GN_TT+"=? AND "+SUBMIT_STATUS+"=? AND "+GN_PICTURE+" !=? AND "+
				GN_PICTURE+" IS NOT NULL AND "+GN_CAPTION+" !=? AND "+GN_CAPTION+" IS NOT NULL",
				new String[]{idtt, submit_status, "", ""}, null, null, ORDER+" ASC");
	}
	
	public synchronized void updateStatusDataGeneral(int id_lokal, int delivery){
		ContentValues cv = new ContentValues();
		cv.put(DELIVERY, delivery);
		db.update(TABEL_GENERAL, cv, ID_LOKAL+"=?", new String[]{id_lokal+""});
	}
	
	/*public synchronized void updateIdTtGeneral(int id_lokal, String idttgeneral){
		ContentValues cv = new ContentValues();
		cv.put(GN_TT_GENERAL, idttgeneral);
		db.update(TABEL_GENERAL, cv, ID_LOKAL + "=?", new String[]{id_lokal + ""});
	}*/
	
	/*======================VANDALISM PICTURE=====================*/

	public synchronized long insertVandalismPicture(String idtt, String caption, String desc, String picture, String datetime, int delivery, String vd_picture_id, int status, String submit_status,
									   double lat, double lon, int order, int used_flag){
		
		ContentValues cv = new ContentValues();
		cv.put(VD_TT, idtt);
		cv.put(VD_CAPTION, caption);
		cv.put(VD_DESC, desc);
		cv.put(VD_PICTURE, picture);
		cv.put(VD_DATETIME, datetime);
		cv.put(DELIVERY, delivery);
		cv.put(VD_PICTURE_ID, vd_picture_id);
		cv.put(VD_UPDATE_STATUS, status);
		cv.put(SUBMIT_STATUS, submit_status);
		cv.put(LAT, lat);
		cv.put(LON, lon);
		cv.put(ORDER, order);
		cv.put(USED_FLAG, used_flag);
		return db.insert(TABEL_VANDALISM_PICTURE, "", cv);
	}

	public synchronized void updateVandlismPicture(int id_lokal, String picture, String datetime,
												   double lat, double lon){

		ContentValues cv = new ContentValues();
		cv.put(VD_PICTURE, picture);
		cv.put(VD_DATETIME, datetime);
		cv.put(DELIVERY, Delivery.PENDING);
		cv.put(VD_UPDATE_STATUS, Gambar.IMAGE_UPDATE);
		cv.put(LAT, lat);
		cv.put(LON, lon);
		db.update(TABEL_VANDALISM_PICTURE, cv, ID_LOKAL + "=?", new String[]{id_lokal + ""});
	}
	
	public synchronized void updateVandlismPicture(int id_lokal, String caption, String picture, int delivery,
									  int order, int used_flag){
		
		ContentValues cv = new ContentValues();
		cv.put(VD_CAPTION, caption);
		cv.put(VD_PICTURE, picture);
		cv.put(DELIVERY, delivery);
		cv.put(ORDER, order);
		cv.put(USED_FLAG, used_flag);
		db.update(TABEL_VANDALISM_PICTURE, cv, ID_LOKAL + "=?", new String[]{id_lokal + ""});
	}
	
	public synchronized void updateStatusUpdateVandalismPicture(int id_lokal, int status){
		ContentValues cv = new ContentValues();
		cv.put(VD_UPDATE_STATUS, status);
		db.update(TABEL_VANDALISM_PICTURE, cv, ID_LOKAL+"=?", new String[]{id_lokal+""});
	}
	
	public synchronized Cursor getVandalismPicture(String idtt, int delivery, String submit_status){
		return db.query(TABEL_VANDALISM_PICTURE, null, VD_TT+"=? AND "+DELIVERY+"=? AND "+SUBMIT_STATUS+"=?",
				new String[]{idtt, delivery+"", submit_status}, null, null, ID_LOKAL+" ASC");
	}
	
	public synchronized Cursor getSingleVandalismPicture(String idtt, int idlokal){
		return db.query(TABEL_VANDALISM_PICTURE, null, VD_TT+"=? AND "+ID_LOKAL+"=?", new String[]{idtt, idlokal+""}, null, null, ID_LOKAL+" ASC");
	}
	
	public synchronized Cursor getVandalismPicture(String idtt, String submit_status){
		return db.query(TABEL_VANDALISM_PICTURE, null, VD_TT+"=? AND "+SUBMIT_STATUS+"=?", new String[]{idtt, submit_status}, null, null, ID_LOKAL+" ASC");
	}

	public synchronized Cursor getVandalismPictureWithImage(String idtt, String submit_status){
		return db.query(TABEL_VANDALISM_PICTURE, null, VD_TT+"=? AND "+SUBMIT_STATUS+"=? AND "+VD_PICTURE+" !=? AND "+
						VD_PICTURE+" IS NOT NULL AND "+VD_CAPTION+" !=? AND "+VD_CAPTION+" IS NOT NULL",
				new String[]{idtt, submit_status, "", ""}, null, null, ORDER+" ASC");
	}
	
	public synchronized void updateStatusVandalismPicture(int id_lokal, int delivery){
		ContentValues cv = new ContentValues();
		cv.put(DELIVERY, delivery);
		db.update(TABEL_VANDALISM_PICTURE, cv, ID_LOKAL + "=?", new String[]{id_lokal + ""});
	}
	
	/*public synchronized void updateVandalismPictureId(int id_lokal, String pictureid){
		ContentValues cv = new ContentValues();
		cv.put(VD_PICTURE_ID, pictureid);
		db.update(TABEL_VANDALISM_PICTURE, cv, ID_LOKAL+"=?", new String[]{id_lokal+""});
	}*/
	
	public synchronized void insertDamaged(String idtt, String owner, String remark, String tenant, String datetime, int delivery){
		
		ContentValues cv = new ContentValues();
		cv.put(DM_TT, idtt);
		cv.put(DM_OWNER, owner);
		cv.put(DM_REMARK, remark);
		cv.put(DM_TENANT, tenant);
		cv.put(DM_DATETIME, datetime);
		cv.put(DELIVERY, delivery);
		db.insert(TABEL_DAMAGED, "", cv);
	}

	public synchronized Cursor getDamaged(String idtt, int delivery){
		return db.query(TABEL_DAMAGED, null, DM_TT+"=? AND "+DELIVERY+"=?", new String[]{idtt, delivery+""}, null, null, ID_LOKAL+" ASC");
	}
	
	public synchronized Cursor getLastDamaged(String idtt){
		return db.query(TABEL_DAMAGED, new String[]{DM_OWNER, DM_TENANT, DM_REMARK}, DM_TT+"=?", new String[]{idtt}, null, null, ID_LOKAL+" ASC");
	}
	
	public synchronized void updateStatusDamaged(int id_lokal, int delivery){
		ContentValues cv = new ContentValues();
		cv.put(DELIVERY, delivery);
		db.update(TABEL_DAMAGED, cv, ID_LOKAL + "=?", new String[]{id_lokal + ""});
	}
	
	/////////////////////////////METAAA
	public synchronized void insertAndUpdateMeta(String idtt, String name, String value, String type, String datetime, int delivery, String flag,
									double lat, double lon){
		ContentValues cv = new ContentValues();
		cv.put(MT_VALUE, value);
		cv.put(MT_TYPE, type);
		cv.put(MT_DATETIME, datetime);
		cv.put(DELIVERY, delivery);
		cv.put(MT_FLAG, flag);
		cv.put(LAT, lat);
		cv.put(LON, lon);

		int row = db.update(TABEL_META, cv, MT_TT+"=? AND "+MT_NAME+"=?", new String[]{idtt, name});
		if(row == 0){
			cv.put(MT_TT, idtt);
			cv.put(MT_NAME, name);
			db.insert(TABEL_META, "", cv);
		}
	}

	public synchronized void insertAndUpdateMetaNoLatLonUpdate(String idtt, String name, String value, String type, String datetime, int delivery, String flag,
															   double lat, double lon){
		ContentValues cv = new ContentValues();
		cv.put(MT_VALUE, value);
		cv.put(MT_TYPE, type);
		cv.put(DELIVERY, delivery);
		cv.put(MT_FLAG, flag);

		int row = db.update(TABEL_META, cv, MT_TT+"=? AND "+MT_NAME+"=?", new String[]{idtt, name});
		if(row == 0){
			cv.put(LAT, lat);
			cv.put(LON, lon);
			cv.put(MT_DATETIME, datetime);
			cv.put(MT_TT, idtt);
			cv.put(MT_NAME, name);
			db.insert(TABEL_META, "", cv);
		}
	}
	
	public synchronized Cursor getMetaNameLike(String idtt, String name){
		return db.query(TABEL_META, null, MT_TT+"=? AND "+MT_NAME+" LIKE ?", new String[]{idtt, "%"+name+"%"}, null, null, MT_NAME+" ASC");
	}

	/*public synchronized Cursor getAllMeta(String idtt){
		return db.query(TABEL_META, null, MT_TT+"=?", new String[]{idtt}, null, null, ID_LOKAL+" ASC");
	}*/
	
	public synchronized void deleteMetaByIdtt(String idtt){
		db.delete(TABEL_META, MT_TT+"=? AND ( "+DELIVERY+"=? OR "+DELIVERY+"=? )", new String[]{idtt, Delivery.SUCCEED+"", Delivery.FAILED+""});
	}
	
	public synchronized void deleteMetaByIdtt(String idtt, String name){
		db.delete(TABEL_META, MT_TT+"=? AND "+MT_NAME+"=?", new String[]{idtt, name});
	}

	public synchronized Cursor getAllMeta(String idtt, String flag){
		return db.query(TABEL_META, null, MT_TT+"=? AND "+MT_FLAG+"=?", new String[]{idtt, flag}, null, null, ID_LOKAL+" ASC");
	}

	public synchronized Cursor getAllMetaNotImage(String idtt, int delivery, String flag){
		return db.query(TABEL_META, null, MT_TT+"=? AND "+MT_TYPE+"!=? AND "+DELIVERY+"=? AND "+MT_FLAG+"=?",
				new String[]{idtt, "image", delivery+"", flag}, null, null, ID_LOKAL+" ASC");
	}
	
	public synchronized Cursor getAllMetaImage(String idtt, int delivery, String flag){
		return db.query(TABEL_META, null, MT_TT+"=? AND "+MT_TYPE+"=? AND "+DELIVERY+"=? AND "+MT_FLAG+"=?",
				new String[]{idtt, "image", delivery+"", flag}, null, null, ID_LOKAL+" ASC");
	}

	public synchronized Cursor getAllMetaImage(String idtt, String flag){
		return db.query(TABEL_META, null, MT_TT+"=? AND "+MT_TYPE+"=? AND "+MT_FLAG+"=?",
				new String[]{idtt, "image", flag}, null, null, ID_LOKAL+" ASC");
	}

	public synchronized Cursor getSingleMeta(String idtt, String name){
		return db.query(TABEL_META, null, MT_TT+"=? AND "+MT_NAME+"=?", new String[]{idtt, name}, null, null, ID_LOKAL+" ASC");
	}
	
	public synchronized void updateStatusSingleMeta(String idtt, String name, int delivery){
		ContentValues cv = new ContentValues();
		cv.put(DELIVERY, delivery);
		db.update(TABEL_META, cv, MT_TT+"=? AND "+MT_NAME+"=?", new String[]{idtt, name});
	}
	
	public synchronized void updateStatusMultipleMeta(String idtt, int delivery, String... name){
		String where = "(";
		for(int i=0; i<name.length; i++){
			where += (",'"+name[i]+"'");
		}
		where = where.replaceFirst(",", "") + ")";

		String sql = "UPDATE "+TABEL_META+" SET "+DELIVERY+"="+delivery;
		sql += " WHERE "+MT_TT+"="+idtt;
		sql += " AND " + MT_NAME+" IN "+where;

		db.execSQL(sql);
	}
	
	public synchronized Cursor getTTOverview(){
		return db.query(TABEL_OV, new String[]{OV_STATUS, OV_COUNT}, null, null, null, null, null);
	}
	
	public synchronized void insertTTOverview(String status, int count){
		ContentValues cv = new ContentValues();
		cv.put(OV_STATUS, status);
		cv.put(OV_COUNT, count);
		db.insert(TABEL_OV, "", cv);
	}
	
	public synchronized int getStatusDeliveryTT(String idtt){
		
		int status = Delivery.SUCCEED;
		Cursor cc = db.query(TABEL_TT, new String[]{DELIVERY}, TT_ID+"=?", new String[]{idtt}, null, null, null, "0, 1");
		if(cc.moveToNext())
			status = cc.getInt(0);
		cc.close();

		return status;
	}
	
	public synchronized String getTTMsg(String idtt){
		String msg = "";
		Cursor cc = db.query(TABEL_TT, new String[]{TT_MSG}, TT_ID + "=?", new String[]{idtt}, null, null, null);
		if (cc.moveToNext()) msg = cc.getString(0);
		cc.close();

		return msg;
	}
	
	public synchronized Cursor getTT(String status, int start, int offset, String keyword){
		String where;
		String arg[];
		if((! TextUtils.isEmpty(status)) && (! TextUtils.isEmpty(keyword)) ) {
			status = status.toLowerCase();
			where = TT_STATUS + "=? AND "+TT_JSON+" LIKE ?";
			arg = new String []{status, "%"+keyword+"%"};
		}
		else if(! TextUtils.isEmpty(status)){
			status = status.toLowerCase();
			where = TT_STATUS + "=?";
			arg = new String []{status};
		}
		else{
			where = TT_JSON + " LIKE ?";
			arg = new String []{"%"+keyword+"%"};
		}
		return db.query(TABEL_TT, null, where, arg, null, null, TT_ID+" DESC", start+", "+offset);

	}
	
	public synchronized Cursor getTTTypeAndCase(String idtt){
		return db.query(TABEL_TT, new String[]{TT_TYPE, TT_CASE, TT_KRONOLOGI, TT_DESC}, TT_ID+"=?", new String[]{idtt}, null, null, null);
	}
	
	public synchronized Cursor getTT(int delivery){
		return db.query(TABEL_TT, new String[]{TT_ID, TT_TYPE, TT_CASE, TT_KRONOLOGI, TT_DESC, TT_SUBMISSION_TIME, TT_JSON}, DELIVERY+"=?", new String[]{delivery+""}, null, null, null);
	}
	
	public synchronized Cursor getTTJson(String idtt){
		return db.query(TABEL_TT, new String[]{TT_JSON}, TT_ID+"=?", new String[]{idtt}, null, null, null);
	}

	public synchronized void updateTTStatusDelivery(String idtt, int status_delivery){
		ContentValues cv = new ContentValues();
		cv.put(DELIVERY, status_delivery);
		db.update(TABEL_TT, cv, TT_ID+"=?", new String[]{idtt});
	}
	
	public synchronized void updateTTSubmissionTime(String idtt, String submission_time){
		ContentValues cv = new ContentValues();
		cv.put(TT_SUBMISSION_TIME, submission_time);
		db.update(TABEL_TT, cv, TT_ID+"=?", new String[]{idtt});
	}
	
	public synchronized void updateTTTypeAndCase(String idtt, String tt_type, String tt_case, int kronologi){
		ContentValues cv = new ContentValues();
		cv.put(TT_TYPE, tt_type);
		cv.put(TT_CASE, tt_case);
		cv.put(TT_KRONOLOGI, kronologi);
		db.update(TABEL_TT, cv, TT_ID+"=?", new String[]{idtt});
	}
	
	public synchronized void updateTTJson(String idtt, String json){
		ContentValues cv = new ContentValues();
		cv.put(TT_JSON, json);
		db.update(TABEL_TT, cv, TT_ID+"=?", new String[]{idtt});
	}
	
	public synchronized void emptyAllTable(){
		Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
		while(c.moveToNext()) {
			db.delete(c.getString(0), null, null);
		}
		c.close();
	}
	
}
