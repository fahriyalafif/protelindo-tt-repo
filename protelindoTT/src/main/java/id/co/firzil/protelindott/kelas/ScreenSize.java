package id.co.firzil.protelindott.kelas;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;

public class ScreenSize {
	private DisplayMetrics metrics = new DisplayMetrics();
	
	public ScreenSize(Context c){
		((Activity)c).getWindowManager().getDefaultDisplay().getMetrics(metrics);
	}
	
	public int getWidth(){
		return metrics.widthPixels;
	}
	
	public int getHeigth(){
		return metrics.heightPixels;
	}
	
}
