package id.co.firzil.protelindott.activity;

import java.io.File;

import org.json.JSONObject;

import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.kelas.AksesData;
import id.co.firzil.protelindott.kelas.Alert;
import id.co.firzil.protelindott.kelas.AsyncProcess;
import id.co.firzil.protelindott.kelas.Config;
import id.co.firzil.protelindott.kelas.Constants;
import id.co.firzil.protelindott.kelas.DbHandler;
import id.co.firzil.protelindott.kelas.Delivery;
import id.co.firzil.protelindott.kelas.ErrorWriter;
import id.co.firzil.protelindott.kelas.Flag;
import id.co.firzil.protelindott.kelas.LatLonValidation;
import id.co.firzil.protelindott.kelas.ScreenSize;
import id.co.firzil.protelindott.kelas.TakeImage;
import id.co.firzil.protelindott.kelas.ThumbnailImage;
import id.co.firzil.protelindott.kelas.URL;
import id.co.firzil.protelindott.kelas.Utils;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

@SuppressLint("InflateParams")
public class SelfieActivity extends BaseFragmentActivity{
	private View save, take_selfie;
	private ImageView gambar;
	private String path_gambar;
	private TakeImage ti;
	private String idtt;
	private boolean sudah_selfie = false, edit_selfie = false;
	int w;
	
	protected void onCreate(Bundle b){
		super.onCreate(b);
		
		setJudul("TT Form");
		data_tt = getIntent().getStringExtra(Constants.DATA_TT);
		
		try{
			JSONObject j = new JSONObject(data_tt);
			idtt = j.getString("idtt");
			/*site_id = j.getString("protelindo_site_id");
			site_name = j.getString("protelindo_site_name");
			latlon = j.getString("Latitude")+", "+j.getString("Longitude");*/
		}
		catch(Exception e){
			e.printStackTrace();
			notif_error_parsing_data_tt();
		}
		
		View m = inf.inflate(R.layout.selfie, null);
		save = m.findViewById(R.id.save);
		take_selfie = m.findViewById(R.id.take_selfie);
		gambar = (ImageView) m.findViewById(R.id.gambar);
		
		ScreenSize ss = new ScreenSize(c);
		w = ss.getWidth();
		int h = ss.getHeigth();
		if(w > h) w = h;
		
		LayoutParams lp_gambar = new LayoutParams(w, w);
		lp_gambar.addRule(RelativeLayout.CENTER_IN_PARENT);
		gambar.setLayoutParams(lp_gambar);
		
		save.setOnClickListener(this);
		take_selfie.setOnClickListener(this);
		
		setMainLayout(m);
		
		runAsyncProcessBackground("Loading data..", new AsyncProcess.OnAksesData() {
			private JSONObject j;
			private boolean selfie_server = false;
			
			@Override
			public void onPreExecute() {}
			
			@Override
			public void onPostExecute() {
				try{
					if(j.optInt("flag") == 1){
						sudah_selfie = true;
						
						/*JSONObject data_meta = j.getJSONObject("data_meta");
						String v = data_meta.getString("meta_value");
						String n = data_meta.getString("meta_name");
						String t = data_meta.getString("meta_input_type");
						
						if(selfie_server){
							DbHandler.get(c).insertAndUpdateMeta(idtt, n, v, t, "", Delivery.SUCCEED, Flag.SELFIE, 0, 0);
						}
						
						Log.d("selfie lewat", "selfie lewat = "+v);*/
						
						/*String path = v.startsWith("/") ? ("file://"+v) : URL.BASE_PHOTO + v;
						new ImageApik(c).displayImage(path, gambar);*/
						
						startActivityAndFinish(new Intent(c, OpenFormActivity.class).putExtra(Constants.DATA_TT, data_tt));
					}
				}
				catch(Exception e){
					e.printStackTrace();
					//if(online()) notif_server_error();
				}
			}
			
			@Override
			public void onDoInBackground() {
				Cursor cc = DbHandler.get(c).getSingleMeta(idtt, "selfie");
				if(cc.moveToNext()){
					try{
						j = new JSONObject();
						j.put("flag", 1);
						JSONObject kl = new JSONObject();
						kl.put("meta_name", cc.getString(2));
						kl.put("meta_value", cc.getString(3));
						kl.put("meta_input_type", cc.getString(4));
						j.put("data_meta", kl);
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
				/*else if(online()){
					selfie_server = true;
					AksesData ak = new AksesData();
					ak.addParam("type", "tt");
					ak.addParam("id", idtt);
					ak.addParam("name", "selfie");
					
					j = ak.getJSONObjectRespon(URL.SINGLE_META, "GET");
				}*/
				cc.close();
				
			}
		});
	}
	
	protected void onActivityResult(int request, int result, Intent d){
		super.onActivityResult(request, result, d);
		if(result == RESULT_OK){
			if(request == Constants.PICK_FROM_CAMERA){
				try{
					ThumbnailImage th = new ThumbnailImage();
					String path_asli = ti.processResultAndReturnImagePathCamera();
					
					String path_baru = Utils.convertBitmapToFilePath(c, th.loadBitmap(path_asli, config.getDataInt(Config.MIN_IMAGE_WIDTH), config.getDataInt(Config.MIN_IMAGE_HEIGHT)));
					Utils.hapusFile(path_asli);
					
					path_asli = path_baru;

					File f = new File(path_asli);
					long size = f.length() / 1000;

					if(size <= config.getDataInt(Config.MAX_FILE_SIZE)){
						path_gambar = path_asli;
						gambar.setImageBitmap(th.loadBitmap(path_gambar, config.getDataInt(Config.MIN_IMAGE_WIDTH) / 2, config.getDataInt(Config.MIN_IMAGE_HEIGHT) / 2));

						edit_selfie = true;
					}
					else {
						f.delete();
						notif("Max file size is "+config.getDataInt(Config.MAX_FILE_SIZE) +" KB");
					}
				}
				catch(OutOfMemoryError o){
					o.printStackTrace();
                    Alert.show(c, "Sorry, Out Of Memory Error", o.getMessage(), null);
                }
				catch(NullPointerException o){
					o.printStackTrace();
                    Alert.show(c, "Sorry, Null Pointer Exception", "Unable to complete previous operation\ndue to low memory", null);
				}
				catch(Exception o){
					o.printStackTrace();
					startErrorActivity(ErrorWriter.getError(o));
				}
    		}
		}
	}
	
	public void onClick(View v){
		if(v == save){
			if(sudah_selfie){
				if(edit_selfie) upload();
				else startActivityAndFinish(new Intent(c, OpenFormActivity.class).putExtra(Constants.DATA_TT, data_tt));
			}
			else if(TextUtils.isEmpty(path_gambar)) notif("Please take selfie first");
			else upload();
		}
		else if(v == take_selfie){
			LatLonValidation.validate(c, data_tt, callback);
		}
		else super.onClick(v);
	}

	private LatLonValidation.CallBack callback = new LatLonValidation.CallBack() {
		@Override
		public void onSuccess() {
			if(ti == null) ti = new TakeImage((Activity)c, Utils.getPathDirectory(c));
			ti.startCamera();
		}
	};
	
	private void upload(){
		DbHandler.get(c).insertAndUpdateMeta(idtt, "selfie", path_gambar, "image", time, Delivery.PENDING, Flag.SELFIE, getLat(), getLon());
		//if(online()) startService(new Intent(c, PostBackgroundService.class));
		//notif("Uploading image..");
		startActivityAndFinish(new Intent(c, OpenFormActivity.class).putExtra(Constants.DATA_TT, data_tt));
	}
	
	public void onBackPressed(){
		startActivityAndFinish(new Intent(c, ListTaskActivity.class));
	}
	
}