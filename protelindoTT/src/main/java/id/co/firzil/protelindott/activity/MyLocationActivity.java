package id.co.firzil.protelindott.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONObject;

import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.kelas.AksesData;
import id.co.firzil.protelindott.kelas.AsyncProcess;
import id.co.firzil.protelindott.kelas.Constants;
import id.co.firzil.protelindott.kelas.DbHandler;
import id.co.firzil.protelindott.kelas.URL;
import id.co.firzil.protelindott.kelas.Utils;

@SuppressLint({ "InflateParams"})
public class MyLocationActivity extends BaseFragmentActivity{
	private TextView location, time;
	
	protected void onCreate(Bundle b){
		super.onCreate(b);
		View m = inf.inflate(R.layout.my_location, null);
		setMainLayout(m);

		location = (TextView) m.findViewById(R.id.current_location);
		time = (TextView) m.findViewById(R.id.current_time);

		time.setText("Current time:\n"+Utils.getDeviceTime());
		location.setText("Current Location:\nUnknown");

		setJudul("TT My Location");
	}

	/*private class GPSReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context c, Intent i) {
			try{
				if(i.getAction().equalsIgnoreCase(GPSTracker.INTENT_FILTER)){
					double lat = i.getDoubleExtra(GPSTracker.TAG_LATITUDE, GPSTracker.LAT_UNKNOWN);
					double lon = i.getDoubleExtra(GPSTracker.TAG_LONGITUDE, GPSTracker.LON_UNKNOWN);

					if(lat != GPSTracker.LAT_UNKNOWN) location.setText("Current Location:\n"+lat+", "+lon);
					else location.setText("Current Location:\nUnknown");
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}

	}*/
	
	public void onClick(View v){}

}