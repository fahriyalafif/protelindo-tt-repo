package id.co.firzil.protelindott.kelas;

public class Flag {
	public static final String 
			VANDALISM="flag_vandalism", 
			VANDALISM_DAMAGE="flag_damaged_item",
			AC_CHRONO ="flag_ac_chrono",
			SELFIE = "flag_selfie",
			DESCRIPTION = "flag_description",
			AC_ = "flag_ac";
	
}
