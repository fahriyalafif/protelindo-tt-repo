package id.co.firzil.protelindott.kelas;

import id.co.firzil.protelindott.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class AddDetailTenantLayout {
	@SuppressLint("InflateParams")
	public void add(final Context c, final ViewGroup v, final String name, final String value){
		AsyncProcess ap = new AsyncProcess(c, "");
		ap.showProgressDialog(false);
		ap.setOnAksesData(new AsyncProcess.OnAksesData() {
			@Override
			public void onPreExecute() {}
			
			@Override
			public void onPostExecute() {
				int max = v.getWidth() - v.getPaddingLeft() - v.getPaddingRight();
				
				int kolom1 = (int) (max * 0.35);
				int kolom2 = (int) (max * 0.05);
				int kolom3 = (int) (max * 0.60);
				
				View m = LayoutInflater.from(c).inflate(R.layout.detail_tenant, null);
				TextView nama_data = (TextView) m.findViewById(R.id.nama_data);
				TextView value_data = (TextView) m.findViewById(R.id.nilai_data);
				TextView separator = (TextView) m.findViewById(R.id.separator);
				
				nama_data.setText(name);
				value_data.setText(value.replace("\r\n", "").trim());
				
				nama_data.getLayoutParams().width = kolom1;
				separator.getLayoutParams().width = kolom2;
				value_data.getLayoutParams().width = kolom3;
				
				v.addView(m);
			}
			
			@Override
			public void onDoInBackground() {
				try {
					Thread.sleep(500);
				} 
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		ap.jalankan();
	}
}
