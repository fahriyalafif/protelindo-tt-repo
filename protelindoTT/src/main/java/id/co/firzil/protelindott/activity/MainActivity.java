package id.co.firzil.protelindott.activity;

import org.json.JSONArray;
import org.json.JSONObject;

import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.kelas.AksesData;
import id.co.firzil.protelindott.kelas.AsyncProcess;
import id.co.firzil.protelindott.kelas.DbHandler;
import id.co.firzil.protelindott.kelas.GPSDetector;
import id.co.firzil.protelindott.kelas.URL;
import id.co.firzil.protelindott.kelas.Utils;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

@SuppressLint({ "InflateParams", "DefaultLocale" })
public class MainActivity extends BaseFragmentActivity{

	protected void onCreate(Bundle b){
		super.onCreate(b);

		setJudul("Dashboard");
		View m = inf.inflate(R.layout.main, null);		
		setMainLayout(m);
		
		if(online()){
			AsyncProcess ap = new AsyncProcess(c, "Loading TT overview..");
			ap.setOnAksesData(new AsyncProcess.OnAksesData() {
				JSONObject j;
				
				@Override
				public void onPreExecute() {}
				
				@Override
				public void onPostExecute() {
					try{
						if(j.optInt("flag") == 1){
							JSONArray ar = j.getJSONArray("result");
							DbHandler.get(c).emptyTable(DbHandler.TABEL_OV);
							if(ar.length() > 0){
								for(int i=0; i<ar.length(); i++){
									JSONObject ob = ar.getJSONObject(i);
									String status = ob.getString("sla_event_status").toLowerCase().trim();
									int count = ob.getInt("count");
									
									if(status.contains("severe") || status.contains("major") || status.contains("minor") || status.contains("not sla")){
										DbHandler.get(c).insertTTOverview(status, count);
										addStatus(status, count);
									}
								}
							}
							else notif("No TT");
						}
						else notif("No TT");
					}
					catch(Exception e){
						e.printStackTrace();
						notif("Server error, load TT from device");
						loadTTOverviewFromLokal();
					}
				}
				
				@Override
				public void onDoInBackground() {
					AksesData ak = new AksesData();
					ak.addParam("iduser", me.getIdVendorUser());
					j = ak.getJSONObjectRespon(URL.TT_OVERVIEW, "GET");
				}
			});
			ap.jalankan();
		}
		else {
			notif("No internet connection, load TT from device");
			loadTTOverviewFromLokal();
		}
	}

	private void loadTTOverviewFromLokal(){
		Cursor cc = DbHandler.get(c).getTTOverview();
		while(cc.moveToNext()){
			addStatus(cc.getString(0), cc.getInt(1));
		}
		cc.close();
	}
	
	private void addStatus(String name, int count){
		int id = 0;
		if(name.equalsIgnoreCase("severe"))id = R.id.severe;
        else if(name.equalsIgnoreCase("severe 1")) id = R.id.severe1;
		else if(name.equalsIgnoreCase("severe 2")) id = R.id.severe2;
		else if(name.equalsIgnoreCase("major")) id = R.id.major;
		else if(name.equalsIgnoreCase("major 2")) id = R.id.major2;
		else if(name.equalsIgnoreCase("minor")) id = R.id.minor;
		else if(name.equalsIgnoreCase("minor 2")) id = R.id.minor2;
		else if(name.equalsIgnoreCase("not sla")) id = R.id.not_sla;
		
		try{
			TextView tv = (TextView) f(id);
			tv.setTag(name);
			tv.setText(tv.getText().toString()+" - "+count);
			tv.setVisibility(View.VISIBLE);
			tv.setOnClickListener(this);
		}
		catch(Exception e){
			e.printStackTrace();
			Utils.showNotif(c, "There is not sla event status \""+name+"\" in this list");
		}
		
	}
	
	public void onClick(View v){
		if(v == pp || v == option) super.onClick(v);
		else startActivityAndFinish(new Intent(c, ListTaskActivity.class).putExtra("status", v.getTag().toString()));
	}
	
	public void onBackPressed(){
		super.onBackPressed();
		GPSDetector t = GPSDetector.getInstance();
		t.stop();
		t = null;

		stopGpsService();
	}
	
}
