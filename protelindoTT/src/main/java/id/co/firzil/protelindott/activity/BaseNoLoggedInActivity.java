package id.co.firzil.protelindott.activity;

import id.co.firzil.protelindott.kelas.Utils;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class BaseNoLoggedInActivity extends AppCompatActivity{
	protected Context c;

	protected void onCreate(Bundle b){
		super.onCreate(b);
		c = this;
		ActionBar ab = getSupportActionBar();
		if(ab != null) {
			ab.setHomeButtonEnabled(false);
			ab.hide();
		}
	}

	protected View f(int id){
		return findViewById(id);
	}
	
	protected void notif(String msg){
		Utils.showNotif(c, msg);
	}
	
	protected boolean online(){
		return Utils.isOnLine(c);
	}
	
}
