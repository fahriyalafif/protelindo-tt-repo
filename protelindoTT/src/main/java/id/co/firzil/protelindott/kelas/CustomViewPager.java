package id.co.firzil.protelindott.kelas;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

@SuppressLint("ClickableViewAccessibility")
public class CustomViewPager extends ViewPager{
	private boolean isSwipeEnabled;
	 
	public CustomViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.isSwipeEnabled = true;
	}
	 
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (isSwipeEnabled) {
			return super.onTouchEvent(event);
		}
		else return false;
	}
	 
	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {
		if (isSwipeEnabled) {
			return super.onInterceptTouchEvent(event);
		}
		else return false;
	}
	 
	/**
	* Custom method to enable or disable swipe
	*
	* @param isSwipeEnabled true to enable swipe, false otherwise
	*
	*/
	public void setPagingEnabled(boolean isSwipeEnabled) {
		this.isSwipeEnabled = isSwipeEnabled;
	}
}
