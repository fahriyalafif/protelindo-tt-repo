package id.co.firzil.protelindott.service;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import id.co.firzil.protelindott.kelas.FcmPreference;
import id.co.firzil.protelindott.kelas.Me;
import id.co.firzil.protelindott.kelas.UpdateRegid;

/*
 * Created by Fahriyal Afif on 8/17/2016.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        String regid = FirebaseInstanceId.getInstance().getToken();
        if(regid == null) regid = "";
        System.out.println("Refreshed token: " + regid);

        new FcmPreference(this).setIsRegisteredInServer(false);
        new Me(this).setGcmRegid(regid);

        new UpdateRegid(this).updateSync();

    }
}
