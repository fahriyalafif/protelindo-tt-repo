package id.co.firzil.protelindott.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.Map;

import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.activity.ListTaskActivity;
import id.co.firzil.protelindott.kelas.DbHandler;
import id.co.firzil.protelindott.kelas.Me;
import id.co.firzil.protelindott.kelas.ThumbnailImage;
import id.co.firzil.protelindott.kelas.Utils;

/*
 * Created by Fahriyal Afif on 8/17/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String NEW_TASK = "new_task",
            REPORT_APPROVED = "report_approved",
            REPORT_REJECTED = "report_rejected";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage){
        Context c = this;
        Me me = new Me(c);
        try{
            Map data = remoteMessage.getData();
            System.out.println("fcm message: "+data.toString());

            JSONObject gcm_message = new JSONObject(data.get("message").toString());

            JSONObject j = gcm_message.getJSONObject("data_gcm");
            String pic_recipient = j.getString("pic_recipient");

            if(me.getIdVendorUser().equalsIgnoreCase(pic_recipient)){

                String task = j.getString("task"),
                        msg = j.getString("message"),
                        id_gcm = gcm_message.getString("id_gcm");

                JSONObject data_tt = j.getJSONObject("data_tt");

                int idtt = data_tt.getInt("idtt");
                String status = data_tt.getString("sla_event_status");

                Class<?> cls = ListTaskActivity.class;

                Intent in = new Intent(c, cls);
                in.putExtra("keyword", task);
                in.putExtra("status", status);

                String title;
                if(id_gcm.equalsIgnoreCase(NEW_TASK)) title = "New Task";
                else if(id_gcm.equalsIgnoreCase(REPORT_APPROVED)) title = "Report Approved";
                else if(id_gcm.equalsIgnoreCase(REPORT_REJECTED)) title = "Report Rejected";
                else title = "General Message";

                DbHandler.get(c).insertNotif(idtt);

                createNotif(cls, in, idtt, title, msg);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public void createNotif(Class<?> cls, Intent in, int id_notifikasi, String title, String message){
        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(cls);
        stackBuilder.addNextIntent(in);

        PendingIntent p = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_ONE_SHOT |
                PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.protelindo)
                .setContentTitle(title)
                .setContentText(message)
                .setContentIntent(p)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setAutoCancel(true);
        int h = Utils.dpToPixel(this, 75);
        ThumbnailImage th = new ThumbnailImage();
        builder.setLargeIcon(th.loadBitmap(getResources(), R.drawable.protelindo, h / 2, h / 2));

        Notification notif = builder.build();
        notif.flags |= Notification.FLAG_SHOW_LIGHTS;
        notif.ledOnMS = 1000;
        notif.ledOffMS = 1000;
        notif.defaults = Notification.DEFAULT_LIGHTS;
        notif.defaults |= Notification.DEFAULT_SOUND;

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(id_notifikasi, notif);
    }

}
