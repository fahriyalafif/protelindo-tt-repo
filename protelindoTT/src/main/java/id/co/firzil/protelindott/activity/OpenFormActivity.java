package id.co.firzil.protelindott.activity;

import java.util.ArrayList;

import org.json.JSONObject;

import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.kelas.AddDetailTenantLayout;
import id.co.firzil.protelindott.kelas.Config;
import id.co.firzil.protelindott.kelas.Constants;
import id.co.firzil.protelindott.kelas.DbHandler;
import id.co.firzil.protelindott.kelas.Delivery;
import id.co.firzil.protelindott.kelas.Flag;
import id.co.firzil.protelindott.kelas.Genset;
import id.co.firzil.protelindott.kelas.HandlingType;
import id.co.firzil.protelindott.kelas.LatLonValidation;
import id.co.firzil.protelindott.kelas.TTApproval;
import id.co.firzil.protelindott.kelas.TtStatus;
import id.co.firzil.protelindott.kelas.Utils;
import id.co.firzil.protelindott.service.PostBackgroundService;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

@SuppressLint({ "InflateParams", "DefaultLocale" })
public class OpenFormActivity extends BaseFragmentActivity{
	private TableLayout tl;
	private View histori, add_edit_pict, submit_report, change_padlock;
	private Spinner stype;
	private CheckBox need;
	private EditText desc;
	private String idtt, handling_type, resolution="", evidence="", mechantronicPadlock;
	private boolean type_fired = false;
	private OnItemSelectedListener typeselect = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> av, View arg1, int pos, long arg3) {
			if(type_fired){
				String tipe = stype.getSelectedItem().toString();
				
				int need_kronologi;
				if(tipe.contains(evidence) && handling_type.equalsIgnoreCase(HandlingType.AC) ){
					need.setVisibility(View.VISIBLE);
					need_kronologi = need.isChecked() ? 1 : 0;
				}
				else{
					need.setVisibility(View.GONE);
					need.setChecked(false);
					need_kronologi = 0;
				}
				
				DbHandler.get(c).updateTTTypeAndCase(idtt, tipe, handling_type, need_kronologi);
				
			}
			else type_fired = true;
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {}
	};
		
	protected void onCreate(Bundle b){
		super.onCreate(b);
		
		try{
			setJudul("TT Form");
			data_tt = getIntent().getStringExtra(Constants.DATA_TT);
			
			JSONObject j = new JSONObject(data_tt);
			idtt = j.getString("idtt");
			String current_status = j.getString("tt_current_status"),
					trouble_type = j.getString("suspect_trouble");
			handling_type = j.getString("handling_type");
			mechantronicPadlock = j.optString("MechatronicPadlock", "0");
			
			View m = inf.inflate(R.layout.open_form, null);
			change_padlock = m.findViewById(R.id.change);
			need = (CheckBox) m.findViewById(R.id.cb_need_kronologi);
			tl = (TableLayout) m.findViewById(R.id.detail_tenant);
			TextView tv_case = (TextView) m.findViewById(R.id.kasuss);
			stype = (Spinner) m.findViewById(R.id.spinner_type);
			add_edit_pict = m.findViewById(R.id.add_edit_pict);
			histori = m.findViewById(R.id.status_histori);
			submit_report = m.findViewById(R.id.submit_report);
			desc = (EditText) m.findViewById(R.id.description);
			
			((TextView) m.findViewById(R.id.gate_padlock)).setText("Gate Padlock : " + j.optString("Gatecombination", "").replace("\r\n", "").replace("null", ""));
			
			tv_case.setText(trouble_type);
			
			stype.setOnItemSelectedListener(typeselect);
			
			need.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					String tipe = stype.getSelectedItem().toString();
					DbHandler.get(c).updateTTTypeAndCase(idtt, tipe, handling_type, isChecked ? 1 : 0);
				}
			});
			
			String type_lokal = "";
			int need_kronologi = 0;
			Cursor cc = DbHandler.get(c).getTTTypeAndCase(idtt);
			if(cc.moveToNext()) {
				type_lokal = cc.getString(0);
				need_kronologi = cc.getInt(2);

				
				if(TextUtils.isEmpty(type_lokal)) type_lokal = "";

			}
			cc.close();

			cc = DbHandler.get(c).getSingleMeta(idtt, "tt_description");
			if(cc.moveToNext()){
				desc.setText(cc.getString(3));
			}
			cc.close();

			ArrayList<String> list_type = new ArrayList<>();

			String restore = "";
			if(handling_type.equalsIgnoreCase(HandlingType.GENSET)) {
	            restore = Genset.RESTORE;
	            resolution = Genset.RESOLUTION;
	            evidence = Genset.EVIDENCE;
	        }
	        else{
	            restore = "Restore";
	            resolution = "Resolution";
	            evidence = "Evidence";
	        }
	        
	        if(TTApproval.getApprovalStatus(data_tt) == TTApproval.NOT_YET_APPROVED)
	        	list_type.add("General");
	        
	        else if(current_status.equalsIgnoreCase(TtStatus.RESPONSE)){
	            list_type.add(restore);
	            list_type.add(restore +", "+resolution+", "+evidence);
	            list_type.add("SLA Break Start");
	            list_type.add("General");
	        }
	        else if(current_status.equalsIgnoreCase(TtStatus.RESTORE)){
	            if(handling_type.equalsIgnoreCase(HandlingType.GENSET)) list_type.add(resolution);
	            
	            list_type.add(resolution+", "+evidence);
	            list_type.add("SLA Break Start");
	            list_type.add("General");
	        }
	        else if(current_status.equalsIgnoreCase(TtStatus.RESOLUTION)){
	            list_type.add(evidence);
	            list_type.add("General");
	        }
	        else if(current_status.equalsIgnoreCase(TtStatus.SLA_BREAK_START) || current_status.equalsIgnoreCase(TtStatus.SLA_BREAK)){
	            list_type.add("SLA Break End");
	            list_type.add("General");
	        }
			
	        stype.setAdapter(new ArrayAdapter<>(c, R.layout.custom_spinner_item, list_type));
			for(int i=0; i<list_type.size(); i++){
				if(type_lokal.equalsIgnoreCase(list_type.get(i))){
					stype.setSelection(i);
					break;
				}
			}
			
			if(TextUtils.isEmpty(type_lokal))
				type_lokal = list_type.get(0);
			
			
			need.setVisibility(type_lokal.contains(evidence) && handling_type.equalsIgnoreCase(HandlingType.AC) ? View.VISIBLE : View.GONE);
			need.setChecked(need_kronologi == 1 ? true : false);
			
			String tipe = stype.getSelectedItem().toString();
			DbHandler.get(c).updateTTTypeAndCase(idtt, tipe, handling_type, need_kronologi);
			
			Log.d("tipe lokal = "+tipe, "tipe lokal = "+type_lokal);
			
			String data_tt = getIntent().getStringExtra(Constants.DATA_TT);
			
			add_edit_pict.setOnClickListener(this);
			histori.setOnClickListener(this);
			submit_report.setOnClickListener(this);
			change_padlock.setOnClickListener(this);
			
			setMainLayout(m);
			
			/*if(TextUtils.isEmpty(desc) && TTApproval.isHadBeenRejected(data_tt) && online()){
				AsyncProcess ap = new AsyncProcess(c, "Getting previous data..");
				ap.showProgressDialog(true);
				ap.setOnAksesData(new AsyncProcess.OnAksesData() {
					JSONObject j = null;
					@Override
					public void onPreExecute() {}
					
					@Override
					public void onPostExecute() {
						try{
							if(j.optInt("flag") == 1){	
								JSONObject data_meta = j.getJSONObject("data_meta");
								String v = data_meta.getString("meta_value");
								
								OpenFormActivity.this.desc.setText(v);
								DbHandler.get(c).updateTTDesc(idtt, v);
							}
						}
						catch(Exception e){
							e.printStackTrace();
							notif_server_error();
						}
					}
					
					@Override
					public void onDoInBackground() {
						AksesData ak = new AksesData();
						ak.addParam("type", "tt");
						ak.addParam("id", idtt);
						ak.addParam("name", "tt_description");
						
						j = ak.getJSONObjectRespon(URL.SINGLE_META, "GET");
					}
				});
				ap.jalankan();
			}*/
			
			JSONObject o = new JSONObject(data_tt);
			add("Site ID", o.getString("protelindo_site_id"));
			add("Site Name", o.getString("protelindo_site_name"));
			add("TT Date", o.getString("trouble_ticket_date"));
			add("Resolution Target", o.optString("resolution_target").replace("null", ""));
			add("Tenant", o.getString("tenant"));
			add("TT Number", o.getString("tt_no"));
			
			JSONObject data_pic = o.getJSONObject("data_pic");
			add("Vendor Name", o.getString("OMSubcont").equalsIgnoreCase("null") ? "" : o.getString("OMSubcont"));
			add("PIC Name", data_pic.getString("fullname"));
			add("PIC Phone", data_pic.getString("phone"));
			
		}
		catch(Exception e){
			e.printStackTrace();
			notif_error_parsing_data_tt();
		}
		
	}
	
	private AddDetailTenantLayout a = new AddDetailTenantLayout();
	public void add(String name, String value){
		a.add(c, tl, name, value);
	}
	
	public void onClick(View v){
		if(v == histori){
			startActivityAndFinish(new Intent(c, StatusHistoryActivity.class).putExtra(Constants.DATA_TT, data_tt));
		}
		else if(v == add_edit_pict){
			String tipe = stype.getSelectedItem().toString();
			Class<?> cls;
			if(tipe.contains(evidence) && handling_type.equalsIgnoreCase(HandlingType.VANDALISM)) cls = FillFormVandalismActivity.class;
			else if(tipe.contains(evidence) && handling_type.equalsIgnoreCase(HandlingType.AC) 
					&& need.isShown() && need.isChecked()) cls = FillFormAcActivity.class;
			else cls = AddEditPictGeneralActivity.class;
			
			if(cls == AddEditPictGeneralActivity.class) {
				Log.d("start biasa", "start biasa");
				startActivity(new Intent(c, cls).putExtra(Constants.DATA_TT, data_tt));
			}
			else {
				Log.d("start finish", "start finish");
				startActivityAndFinish(new Intent(c, cls).putExtra(Constants.DATA_TT, data_tt));
			}
		}
		else if(v == submit_report) {
			boolean foto_kurang = false;
			String tipe = stype.getSelectedItem().toString();
			final int min_foto = config.getDataInt(Config.MIN_PHOTO_FOR_APPROVAL_STATUS);
			DbHandler db = DbHandler.get(c);
			if(tipe.contains(resolution) || tipe.contains(evidence) || tipe.equalsIgnoreCase(TtStatus.SLA_BREAK_START)){
				if(handling_type.equalsIgnoreCase(HandlingType.GENSET) || tipe.equalsIgnoreCase(resolution)
						|| tipe.equalsIgnoreCase(TtStatus.SLA_BREAK_START)){
					foto_kurang = db.getDataGeneralWithImage(idtt, tipe).getCount() < min_foto;
				}
				else if(tipe.contains(evidence)){
					if(handling_type.equalsIgnoreCase(HandlingType.VANDALISM))
						foto_kurang = db.getVandalismPictureWithImage(idtt, tipe).getCount() < min_foto;
					else if (handling_type.equalsIgnoreCase(HandlingType.AC)){
						if(need.isChecked()) foto_kurang = db.getAllMetaImage(idtt, Flag.AC_CHRONO).getCount() < min_foto;
						else foto_kurang = db.getDataGeneralWithImage(idtt, tipe).getCount() < min_foto;
					}
					else foto_kurang = db.getDataGeneralWithImage(idtt, tipe).getCount() < min_foto;
				}
			}

            if(TextUtils.isEmpty(desc.getText().toString())) Utils.showNotif(c, "Please insert description");
			else if(foto_kurang) Utils.showNotif(c, "Please take photos at least "+min_foto);
            else {
				LatLonValidation.validate(c, data_tt, new LatLonValidation.CallBack() {
					@Override
					public void onSuccess() {
						DbHandler db = DbHandler.get(c);
						DbHandler.get(c).insertAndUpdateMeta(idtt, "tt_description", desc.getText().toString(), "text", time, Delivery.PENDING, Flag.DESCRIPTION, getLat(), getLon());
						db.updateTTSubmissionTime(idtt, time);
						db.updateTTStatusDelivery(idtt, Delivery.PENDING);
						notif("Submitting report..");

						if (online()) startService(new Intent(c, PostBackgroundService.class));
						kembali();
					}
				});
			}
		}
		else if(v == change_padlock){
			if(mechantronicPadlock == null || (! mechantronicPadlock.equalsIgnoreCase("1") ) )
				startActivityAndFinish(new Intent(c, ChangePadlockActivity.class).putExtra(Constants.DATA_TT, data_tt));
			else notif("Padlock can not changed");
		}
		else super.onClick(v);
	}
	
	public void onDestroy(){
		super.onDestroy();
		//DbHandler.get(c).insertAndUpdateMeta(idtt, "tt_description", desc.getText().toString(), "text", time, Delivery.PENDING, Flag.DESCRIPTION, getLat(), getLon());
	}
	
	public void onBackPressed(){
		kembali();
	}
	
	private void kembali(){
		startActivityAndFinish(new Intent(c, ListTaskActivity.class));
	}
}
