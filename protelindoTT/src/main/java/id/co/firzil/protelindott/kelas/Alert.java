package id.co.firzil.protelindott.kelas;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Alert {
    public interface OkClicked{
        void onClick();
    }

	public static void show(Context c, String title, String msg, final OkClicked okClicked){
        int p = Utils.dpToPixel(c, 8);

        TextView tvMsg = new TextView(c);
        tvMsg.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        tvMsg.setText(msg);
        tvMsg.setPadding(p, p, p, p);
        tvMsg.setTextColor(Color.BLACK);

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(c); 
		//alertDialog.setIcon(R.drawable.delete);
				
		alertDialog.setTitle(title);
        alertDialog.setView(tvMsg);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                if(okClicked != null) okClicked.onClick();
                dialog.dismiss();
            }
        });
        alertDialog.show();
	}
}
