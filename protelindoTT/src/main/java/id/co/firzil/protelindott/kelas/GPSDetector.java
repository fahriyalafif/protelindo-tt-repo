package id.co.firzil.protelindott.kelas;

import android.annotation.TargetApi;
import android.content.Context;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by Rio Rizky Rainey on 26/06/2015.
 * rizkyrainey@gmail.com
 */
public class GPSDetector implements Runnable, LocationListener, GpsStatus.Listener {
    public static final int GPS_TIME = 1, DEVICE_TIME = 2;
    private Listener listener;
    private static GPSDetector main;
    private final ScheduledExecutorService executor;
    private Context activity;
    private long gpsTime, lastGpsTimeUpdate;
    private LocationManager locationManager;
    private ScheduledFuture<?> updateTask;
    public static final double UNKNOWN_POSITION = 1000;
    private double last_lat = UNKNOWN_POSITION, last_lon = UNKNOWN_POSITION;   //berarti belum dapet koordinat aslinya
    private int flag_time = DEVICE_TIME;

    private GPSDetector() {
        this.executor = Executors.newSingleThreadScheduledExecutor();
    }

    public static GPSDetector getInstance() {
        if (main == null) {
            main = new GPSDetector();
        }
        return main;
    }

    public void setActivity(Context paramActivity) {
        activity = paramActivity;
        locationManager = (LocationManager) paramActivity.getSystemService(Context.LOCATION_SERVICE);
    }

    public void setGpsUpdateListener(Listener paramListener) {
        listener = paramListener;
    }

    public void onGpsStatusChanged(int event) {
        switch(event){
            case GpsStatus.GPS_EVENT_STARTED:
                Log.d("LOCATION_LISTENER", "GPS SEARCHING");

                break;
            case GpsStatus.GPS_EVENT_STOPPED:
                Log.d("LOCATION_LISTENER", "GPS STOPPED");

                break;

            case GpsStatus.GPS_EVENT_FIRST_FIX:
                Log.d("LOCATION_LISTENER", "GPS LOCKED");
                break;
            /*case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                Log.d("LOCATION_LISTENER", "GPS SATELITE STATUS");
                break;*/
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void onLocationChanged(Location l) {
        Log.d("LOCATION_LISTENER", "ON LOCATION CHANGED "+l.getLatitude());
        gpsTime = l.getTime();
        last_lat = l.getLatitude();
        last_lon = l.getLongitude();
        flag_time = GPS_TIME;
        lastGpsTimeUpdate = gpsTime;

        //Log.d("MOCKCC", "MOCKCC = "+Utils.isMockLocationEnabled(activity)+" -- "+l.isFromMockProvider());
    }

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    public void resetGpsInfo() {
        gpsTime = 0;
        lastGpsTimeUpdate = 0;
        last_lat = UNKNOWN_POSITION;
        last_lon = UNKNOWN_POSITION;
    }

    public void run() {
        long l = System.currentTimeMillis();
        //if ((gpsStatus > 0) && (l - lastGpsTime > 1000L)) resetGpsInfo();
        //if (gpsStatus == 0) resetGpsInfo();
        if(flag_time == GPS_TIME){
            gpsTime = gpsTime + 1000;
        }
        else {
            flag_time = DEVICE_TIME;
            gpsTime = l;
        }

        if(listener != null) {
            listener.onUpdate(l, gpsTime, lastGpsTimeUpdate, last_lat, last_lon, flag_time);
        }
    }

    public void start(int min_time_update, int min_distance_update) {
        if(updateTask == null) {
            Log.d(getClass().getName(), "started");
            if (this.activity == null)
                throw new Error("Activity must set before start");
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, min_time_update, min_distance_update, this);
            locationManager.addGpsStatusListener(this);
            updateTask = executor.scheduleAtFixedRate(this, 0L, 1L, TimeUnit.SECONDS);
        }
        else Log.d(getClass().getName(), "already started, no need to start");
    }

    public void stop() {
        if(updateTask != null) {
            updateTask.cancel(false);
            locationManager.removeGpsStatusListener(this);
            locationManager.removeUpdates(this);
            resetGpsInfo();
            updateTask = null;
        }
    }

    public interface Listener {
        void onUpdate(long currentTime, long gpsTime, long lastGpsTimeUpdate, double lat, double lon, int flag_time);
    }

    public double getLastLat(){
        return last_lat;
    }

    public double getLastLon(){
        return last_lon;
    }
}