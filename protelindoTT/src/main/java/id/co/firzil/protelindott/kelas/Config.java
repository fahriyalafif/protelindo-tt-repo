package id.co.firzil.protelindott.kelas;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class Config {
	private Context c;
	private static final String CONFIG = "config_prefs", DEFAULT_GMT="GMT+7";

	private static final int
			DEFAULT_RADIUS_TOLERANCE = 300  /*in meter*/,
			DEFAULT_MAX_FILE_SIZE = 100000  /*in KB*/,
			DEFAULT_MIN_TIME_GPS_UPDATE = 3000  /*in miliseconds*/,
			DEFAULT_MIN_DISTANCE_GPS_UPDATE = 1 /*in meter*/,
			DEFAULT_MIN_IMAGE_WIDTH = 1440 /*in pixel*/,
			DEFAULT_MIN_IMAGE_HEIGHT = 1080 /*in pixel*/,
			DEFAULT_MIN_PHOTO_FOR_APPROVAL_STATUS = 0  /*in integer*/;

	public static final String RADIUS_TOLERANCE="radius_tolerance", MAX_FILE_SIZE="max_file_size", MIN_TIME_GPS_UPDATE="min_time_gps_update", GMT="gmt",
			MIN_DISTANCE_GPS_UPDATE="min_distance_gps_update", MIN_IMAGE_WIDTH="min_image_width", MIN_IMAGE_HEIGHT="min_image_height",
			MIN_PHOTO_FOR_APPROVAL_STATUS = "min_photo_for_approval_status";

	private static Config obj;

	public static Config getInstance(Context c){
		if(obj == null) {
			obj = new Config(c);
		}
		return obj;
	}
	private Config(Context c){
		this.c = c;
	}

	public void setDataDefault(){
		Log.d(CONFIG, "SET DATA DEFAULT");
		setData(RADIUS_TOLERANCE, DEFAULT_RADIUS_TOLERANCE);
		setData(MAX_FILE_SIZE, DEFAULT_MAX_FILE_SIZE);
		setData(MIN_TIME_GPS_UPDATE, DEFAULT_MIN_TIME_GPS_UPDATE);
		setData(MIN_DISTANCE_GPS_UPDATE, DEFAULT_MIN_DISTANCE_GPS_UPDATE);
		setData(MIN_IMAGE_WIDTH, DEFAULT_MIN_IMAGE_WIDTH);
		setData(MIN_IMAGE_HEIGHT, DEFAULT_MIN_IMAGE_HEIGHT);
		setData(GMT, DEFAULT_GMT);
		setData(MIN_PHOTO_FOR_APPROVAL_STATUS, DEFAULT_MIN_PHOTO_FOR_APPROVAL_STATUS);
	}

	private SharedPreferences getSharedPreferences() {
		return c.getSharedPreferences(CONFIG, Context.MODE_PRIVATE);
	}

	public void setData(String k, String v){
		getSharedPreferences().edit().putString(k, v).commit();
		Log.d(CONFIG, "SET DATA STRING " + k + " --- " + v);
	}

	public void setData(String k, int v){
		getSharedPreferences().edit().putInt(k, v).commit();
		Log.d(CONFIG, "SET DATA INT " + k + " --- " + v);
	}

	public String getData(String key) {
		String value = getSharedPreferences().getString(key, "");
		//Log.d(CONFIG, "GET DATA "+key+" --- "+value);
		return value;
	}

	public int getDataInt(String key) {
		int value = getSharedPreferences().getInt(key, 0);
		//Log.d(CONFIG, "GET DATA "+key+" --- "+value);
		return value;
	}

	public void clear(){
		getSharedPreferences().edit().clear().commit();
	}

}