package id.co.firzil.protelindott.kelas;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;

import id.co.firzil.protelindott.R;

public class DevOptions {
	private Context c;
	private AlertDialog.Builder alertDialog;
	
	public DevOptions(Context c){
		this.c = c;
	}
	
	public void showDialogDevOptions(){
		if(alertDialog == null){
			alertDialog = new AlertDialog.Builder(c);
			//alertDialog.setIcon(R.drawable.delete);
			String title = "Developer Options", 
					msg = "Jangan centang pilihan \"Don't keep activities\" / \"Jangan simpan kegiatan\" agar "+c.getString(R.string.app_name)+" berjalan lancar.",
					no = "Tidak", yes = "Ya";
					
			alertDialog.setTitle(title);
	        alertDialog.setMessage(msg);
	        alertDialog.setNegativeButton(no, null);
	        alertDialog.setPositiveButton(yes, new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog,int which){
	            	dialog.dismiss();
	            	Intent i = new Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS);
	                i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
	                c.startActivity(i);
	            }
	        });
		}
		alertDialog.show();
	}
	
	@SuppressWarnings("deprecation")
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	public boolean isAlwaysFinishActivitiesOptionEnabled() {
	    int alwaysFinishActivitiesInt = 0;
	    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
	        alwaysFinishActivitiesInt = Settings.System.getInt(c.getContentResolver(), Settings.Global.ALWAYS_FINISH_ACTIVITIES, 0);
	    } else {
	        alwaysFinishActivitiesInt = Settings.System.getInt(c.getContentResolver(), Settings.System.ALWAYS_FINISH_ACTIVITIES, 0);
	    }

	    if (alwaysFinishActivitiesInt == 1) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
}
