package id.co.firzil.aksesserverlib;

public class FileParam {
	public String param_name = "";
	public String path_file = "";
	
	public FileParam(String param_name, String file_path){
		this.param_name = param_name;
		this.path_file = file_path;
	}
}
