package id.co.firzil.aksesserverlib;

public class StringParam {
	public String nama = "";
	public String value = "";
	
	public StringParam(String nama, String value){
		this.nama = nama;
		this.value = value;
	}
}
